## GitLab Group Page

this is the code repository for

https://tutorials6.gitlab.io/processing_p5js/

as the index.html is now made with a template from HTML5 UP 

many files and subdirectories relate to that template and 

NOT to the real P5.js code file repository

these are named: step_x.html and step_x.js
