//________________________________________ STEP 7: EXTRA JS FILE (P5)

//________________________________________ GLOBAL VARIABLES
//........................................ Because our button class now not in sketch.js
//........................................ this 'main' code gets real short.
let button;


function setup() { //_____________________ SETUP
  createCanvas(400, 400);
  button = new Button(10,10,40,40); //____ (1)
}

function draw() { //______________________ DRAW 
  background(200, 200, 0);
  logo(width - 100, 20, 40, 0.015); //___ logo( x, y, radius, speed+delta-ang-per-frame ); // (1)
  button.draw(); // (1)
}

//________________________________________ remark (1) see file myTools.js
