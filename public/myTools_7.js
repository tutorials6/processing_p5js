/*
 myTools  KLL
 use extra js file here in root
 name: myTools.js
 idea is to develop it here ( master ) and download / upload to other projects.
 content: 
 classes declared:
 
 class Button(x,y,w,h)
 declare:
 button = new Button(x,y,w,h);
 
  use:
  button.draw();
 
 functions to call:

 logo(x,y,radius,speed);


//_________________________________ !!
<!-- 
 need 
    <script src="myTools.js"></script>  
 inside index.html
-->

*/

//________________________________________________________ FUNCTION LOGO
var ang = 0;

function logo(x, y, r, dang) {
  var d1 = 2 * r;
  var d2 = 2 * r / sqrt(2);
  ang += dang; //__________________ animation
  push();
  fill(255); //____________________ fill same all 3 shapes
  strokeWeight(4);
  stroke(200, 200, 0); //__________ here same as main background gives a nice effect
  ellipseMode(CENTER);
  rectMode(CENTER);
  translate(x + r, y + r); //______ CENTER thinking
  push();
  rotate(-ang); //__________________ animate first bigger rect
  rect(0, 0, d1, d1);
  pop();
  ellipse(0, 0, d1, d1);
  rotate(ang); //_________________ animate second smaller rect  
  rect(0, 0, d2, d2);
  textAlign(CENTER,CENTER);
  textSize(20);
  text("K L L",0,0);
  pop();
}

//_______________________________________________________ CLASS BUTTON
//....................................................... this is not a real button code, just a rect as class, to show that classes can be stored here
//....................................................... while they are used from main 

class Button{
  constructor(x, y, w, h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }
  
  draw() {
    push();
    //fill(200,200,200);
    noFill();
    stroke(200,200,200);
    strokeWeight(3);
    //noStroke();
    rect(this.x,this.y,this.w,this.h); 
    pop();
  }

}
