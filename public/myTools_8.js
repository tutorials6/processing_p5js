/*
 myTools  KLL
 use extra js file here in assets
 name: myTools.js
 idea is to develop it here ( master ) and download / upload to other projects.
 content: 
 __________________________________ functions to call:

 logo(x,y,radius,speed_angle_change_per_frame);

//_________________________________ !!
 need 
     <script src="myTools.js"></script>
 inside index.html
*/

//___________________________________________________
//  logo(x,y,radius,speed_angle_change_per_frame);

var ang = 0; //____________________ animated LOGO

function logo(x, y, r, dang) {
  var d1 = 2 * r;
  var d2 = 2 * r / sqrt(2);
  ang += dang; //__________________ animation
  push();
  fill(255); //____________________ fill same all 3 shapes
  strokeWeight(4);
  stroke(200, 200, 0); //__________ here same as main background gives a nice effect
  ellipseMode(CENTER);
  rectMode(CENTER);
  translate(x + r, y + r); //______ CENTER thinking
  push();
  rotate(ang); //__________________ animate first bigger rect
  rect(0, 0, d1, d1);
  pop();
  ellipse(0, 0, d1, d1);
  rotate(-ang); //_________________ animate second smaller rect  
  rect(0, 0, d2, d2);
  textAlign(CENTER,CENTER);
  textSize(20);
  text("K L L",0,0);
  pop();
}
