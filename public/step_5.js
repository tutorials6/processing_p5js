//________________________________________ STEP 5: DECIDING THE PROGRAM FLOW / SCREENS and STATES (P5)
//........................................ a state frame work with
//........................................ + intro page start [1] back [space]
//........................................ + button page [1] [9] later 30 buttons
//........................................ + question page for the selected question multipe choice [a][b][c]
//........................................ + answer page after the answer a b c check if correct for state & question, or wrong or typo

//________________________________________ GLOBAL VARIABLES
//........................................ The global variables are used to decide which screen and content to show. 
//........................................ If the variable 'diagp' is true, diagnostic info is showed in the left bottom
//........................................ of the screen. This can be a handy while developing code.
let state = 0; //_________________________ decide starting screen
let question = -1; //_____________________ remember which question the user selected
let answer = ""; //_______________________ remember which answer the user gave
let diagp = true; //______________________ show diagnostic info

function setup() { //_____________________ SETUP
  createCanvas(400, 400);
  print("this sketch is part of a tutorial: http://kll.engineering-news.org/kllfusion01/articles.php?article_id=166");
}

function draw() { //______________________ DRAW
  //...................................... Inside 'draw' the program flow is decided. Based on the values of the global
  //...................................... variables a certain function is executed. 
  background(200, 200, 0);
  if (state === 0)       intro();
  else {
    if (question == -1) buttons();
    else {
      if (answer === "") questions();
      if (answer > "")  answers();
    }
  }
  if (diagp) text("state " + state + " , question " + question + " , answer " + answer, 10, height - 5);
  text("restart : key [space]", width - 115, height - 5);
}

function intro() { //_____________________ FUNCTIONS
  text("INTRODUCTION SCREEN \npress key [1] to start the question game", 20, 30);
}

function buttons() {
  //...................................... This function executes once 'state' isn't value 0 anymore. It's the screen 
  //...................................... where the user picks a question.
  background(150, 200, 0);
  if (state == 1) text("SELECT QUESTION \nhere we pick a question with key [1] till [9]", 20, 30);
}

function questions() {
  //...................................... The function 'questions' shows the question the user selected in 'buttons'.
  background(100, 200, 0);
  if (state == 1) text("QUESTION " + question + "\nhere we show question " + question + ". Answer with [a][b][c]", 20, 30);
}

function answers() {
  //...................................... Once the user picked a question, meaning the variable 'question' isn't -1
  //...................................... anymore, the user can answer by pressing key [a], [b] or [c]. For testing
  //...................................... purposes key [b] is the right answer to each question.
  //...................................... If the user presses another key than [a], [b] or [c] it gives feedback.
  if (answer == 'a' || answer == 'b' || answer == 'c') {
    if (state == 1 && answer == 'b') { //_ right answer
      background(0, 200, 200);
      text("RIGHT ANSWER", 20, 60);
    } else { //___________________________ wrong answer
      background(200, 0, 200);
      text("WRONG ANSWER", 20, 60);
    }
    text("restart the sketch using key [ space]", 20, 120);
  } else { //_____________________________ wrong key
    background(200, 0, 0);
    text("answer with [a], [b] or [c]", 20, 60);
  }
  if (state == 1) text("ANSWER " + question + "\nyour answer to question " + question + " was " + answer, 20, 30);
}

function keyPressed() { //________________ OPERATION
  //...................................... We use 'keyPressed' to give the global variables new values.
  if (state == 1 && question == -1) question = key;
  else if (state == 1 && question > -1) answer = key;

  if (state === 0)
    if (key == 1) state = 1;

  if (key == ' ') {
    state = 0;
    question = -1;
    answer = "";
    print("sketch has reset");
  }
}
