//_______________________________________________ STEP 8: COMBINING ALL (P5)

//_______________________________________________ GLOBAL VARIABLES
//............................................... Now we use additional buttons for game operation
let state = 0 //_________________________________ show intro screen
let last_question = -1 //________________________ remember last question ( like button press 0 ..30 )
let last_answer = "" //__________________________ remember last answer
let diagp = true; //_____________________________ show diagnostic info while development

var cnvat; //____________________________________ HTML positioning

//_______________________________________________ Q & A from CSV file loading
let table, rows, cols, qrow;
let good; //_____________________________________ good answer for question in qrow of table
let infile = "assets/qa_8.csv";

//_______________________________________________ question page button setup / variables
let q_many = 30; //______________________________ many buttons
let q_myButtons = []; //_________________________ make a array for buttons of type class Button
let q_mysel = -1; //_____________________________ global memory about the last selected button
let state1,state2,stat3,back,logit,aB,bB,cB; //__ operation buttons
let mymouseClicked = false; //___________________ own boolean
function preload() { //__________________________ get the question list from CSV file in /assets/
  table = loadTable(infile, "csv", "header");
}

function setup() {
  createCanvas(768, 500).parent('canvasposition');
  check_pos(); //________________________________ read where the canvas has its x0,y0 on the HTML page
  setup_CSV(); //________________________________ get questions_answers_list
  init_q_Button_array() //_______________________ question page setup 30 buttons
}

function draw() {
  background(200, 200, 0);
  textSize(15);
  fill(0); //____________________________________ black help texts
  if (state == 0) intro();
  else if (last_question == -1) buttons();
  else if (last_question > -1 && last_answer == "") questions();
  else if (last_question > -1 && last_answer > "") answers();
  textSize(15);
  if (diagp) text("state " + state + " , last_question " + last_question + " , last_answer " + last_answer+" , back : key [space]", 10, height - 5);

}

function intro() {
  textSize(20);
  text("INTRO \n press key [1][2][3] or Button for start questions game", 20, 30);
  logo(width - 100, 20, 40, 0.015); // logo( x, y, radius, speed-delta-ang-per-frame ); // see file myTools.js
  state1.draw(); //______________________________ show select buttons
  if ( state1.selb ) state = 1;
  state2.draw();
  if ( state2.selb ) state = 2;
  state3.draw();
  if ( state3.selb ) state = 3;

}

function buttons() {
  background(150, 200, 0);
  //_____________________________________________ we know state ( and it is > 0 ) so we know what to show
  textSize(20);
  text("SCREEN " + state + " here we SELECT questions 0 .. 29, selected "+q_mysel, 20, 20);
  fill(200,0,0);
  if (state == 1) for ( let j =0;j < 6;j++) text("Cat 1 "+(j+1),20+j*123,40);
  if (state == 2) for ( let j =0;j < 6;j++) text("Cat 2 "+(j+1),20+j*123,40);
  if (state == 3) for ( let j =0;j < 6;j++) text("Cat 3 "+(j+1),20+j*123,40);
  draw_q_buttons();
  back.draw();
  logit.draw();
  if ( logit.selb ) last_question = q_mysel;  //_ go screen question
  if ( back.selb ) reset(); //___________________ go start screen
}

function questions() {
  background(100, 200, 100);
  text("SCREEN " + state + " here we ASK question " + last_question + "\nonly answer with [a][b][c]", 20, 30);
  //_____________________________________________ select from 90 CSV lines by last_question+(state-1)*30
  if (last_question >= 0 && last_question < 30) qrow = table.getRow(last_question+(state-1)*30);
  let yl = 100;
  text("# " + qrow.getString(0), 20, yl);
  yl += 35;
  textSize(30);
  text(" Rubric       :" + qrow.getString(1), 20, yl);
  yl += 35;
  text(" Question: " + qrow.getString(2), 20, yl);
  yl += 35;
  text(" [a] ? " + qrow.getString(3), 20, yl);
  yl += 35;
  text(" [b] ? " + qrow.getString(4), 20, yl);
  yl += 35;
  text(" [c] ? " + qrow.getString(5), 20, yl);
  yl += 35;
  good = qrow.getString(6); // a b c 
  //_____________________________________________ buttons
  aB.draw();
  if ( aB.selb ) last_answer = 'a'; //___________ or key press
  bB.draw();
  if ( bB.selb ) last_answer = 'b';
  cB.draw();
  if ( cB.selb ) last_answer = 'c';
}

function answers() {
  if (last_answer == "a" || last_answer == "b" || last_answer == "c") {
    if ( last_answer == good ) { //______________ RIGHT
      background(0, 200, 200);
      textSize(50);
      text(" WIN ", 20, 160);
      textSize(100);
//      text("$ "+(1 + floor(last_question / 6)) * 200+" $"  , 20,300);
      text("$ "+q_myButtons[last_question].textb+" $"  , 20,300);
//later      text("$ "+table.getString(last_question+(state-1)*30,7) +" $"  , 20,300);
    } else { //__________________________________ WRONG
      background(200, 0, 200);
      textSize(50);
      text(" LOSE ", 20, 160);
      textSize(20);
      text("last_answer " + last_answer + " good " + good, 40, 190);
    }
    back.draw();
    if ( back.selb ) reset(); //_________________ go start screen

  } else { //____________________________________ TYPO ?
    background(200, 0, 0);
  }
  textSize(20);
  text("ANSWERS state " + state + " check: your answer to q " + last_question + " was " + last_answer, 20, 30);

}

function keyPressed() { //_______________________ replaced by button operation
  if (state == 0) { //___________________________ INTRO PAGE  ( only for tests used )
    if (key == 1) state = 1;
    if (key == 2) state = 2;
    if (key == 3) state = 3;
  } else if (state > 0 && state < 4) { //________ QUESTION PAGE  ( only for tests used )
    if (last_question == -1) last_question = key;
    else if (last_question > -1) last_answer = key;
  }
  if (key == ' ') {
    reset();
  }
}

function mousePressed() {
  mymouseClicked = true;  
}

function reset() {
    state = 0;
    last_question = -1;
    last_answer = "";
    q_mysel = -1; //_____________________________ reset also last selected operation buttons
    back.selb = false;
    logit.selb = false;
    state1.selb  = false;
    state2.selb  = false;
    state3.selb  = false;
    aB.selb  = false;
    bB.selb  = false;
    cB.selb  = false;
    //print("RESET");
}

function setup_CSV() {
  if (diagp) print("\nload CSV file: qa.csv from assets");
  rows = table.getRowCount();
  if (diagp) print(rows + ' total rows in table');
  cols = table.getColumnCount()
  if (diagp) print(cols + ' total columns in table');
  let headers = "";
  for (let j = 0; j < cols; j++) headers += table.columns[j] + "_";
  print(headers);
  for (let r = 0; r < rows; r++) {
    let temp = "";
    for (let c = 0; c < cols; c++) {
      temp += table.getString(r, c) + "_";
    }
    if (diagp) print(temp);
  }
}


function check_pos() {
  cnvat = _renderer.position();
  // print("canvas at : x "+cnvat.x+" y "+cnvat.y);
}

//_______________________________________________ question Button setup
function init_q_Button_array() {
  let x0 = 15, y0 = 50, w = 115, h = 70, off = 8, grid = 6;
  for (let i = 0; i < q_many; i++) {
    let xi = x0 + (i % grid) * (w + off);
    let yi = y0 + (floor(i / grid)) * (h + off);
    let wi = w;
    let hi = h;
    let seli = false;
    let texti = " " + ((1 + floor(i / grid)) * 200);
//    let texti = " " + i;
    let idi = i;
    q_myButtons.push(new Button(xi, yi, wi, hi, seli, texti, idi)); // make buttons and save to array
  }
  
  //_____________________________________________ from page intro
  state1 = new Button(width/2-250, height/2, 150, 40, false, "Round 1",   95); //__
  state2 = new Button(width/2- 75, height/2, 150, 40, false, "Round 2",   96); //__
  state3 = new Button(width/2+100, height/2, 150, 40, false, "Tie Break", 97); //__
  //_____________________________________________ from page buttons
  logit = new Button(width/2, height-42, 80, 40, false, " SEL", 98); //____________ from question select
  back  = new Button(width-85, height-42, 80, 40, false, "back", 99); //___________ as key [space]
  //_____________________________________________ from page question multiple choice
  aB = new Button(width/2-250, height/2+100, 150, 40, false, "[a]",   92); //______ [a]
  bB = new Button(width/2- 75, height/2+100, 150, 40, false, "[b]",   93); //______ [b]
  cB = new Button(width/2+100, height/2+100, 150, 40, false, "[c]",   94); //______ [c]

}

function draw_q_buttons() {
  for (let i = 0; i < q_many; i++) {
    if (i != q_mysel) q_myButtons[i].selb = false;           // only one ( last ) can be selected ( optionsgroup thinking )
    q_myButtons[i].draw();
  }
}


/*
// ______________________________________________ classes declared:
 
 class Button
 declare:
  Button button(x, y, w, h, sel, text, id);
 methods:
  button.draw(); //______________________________ from draw
  button.mousePressed(); //______________________ local
  boolean button.over(); //______________________ local
  
  sets:
  q_mysel //_____________________________________ a needed global for the 30 buttons page
*/

class Button { //________________________________ begin class
  constructor(xi, yi, wi, hi, seli, atexti, idi) {
    this.xb = xi;
    this.yb = yi;
    this.wb = wi;
    this.hb = hi;
    this.selb = seli;
    this.textb = atexti;
    this.idb = idi;
  }

  draw() {
    this.mousePressed();
    strokeWeight(3);
    if (this.selb) fill(0, 200, 0);
    else fill(0, 0, 200);
    if (this.over()) stroke(200, 0, 200);
    else stroke(0, 200, 200);
    rect(this.xb, this.yb, this.wb, this.hb);
    noStroke();
    fill(200);
    textSize(30);
    text(this.textb, this.xb + 3, this.yb + this.hb / 2 + 8);
  }

  over() {
    return (mouseX > this.xb & mouseX < this.xb + this.wb & mouseY > this.yb & mouseY < this.yb + this.hb);
  }

  mousePressed() {
//    if (this.over() && mouseIsPressed) {
    if (this.over() && mymouseClicked) {
      mymouseClicked = false; // reset global
      this.selb = true;
      if ( this.idb < 30 ) q_mysel = this.idb; // set a global 0 .. 29
    }
  }

} //_____________________________________________ end class
